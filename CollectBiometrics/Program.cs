﻿using Afisteam.Biometrics;
using Afisteam.Biometrics.Gui;
using Afisteam.Biometrics.Scannning;
using EPMS_Forensics_Proxy;
using System;
using System.IO;
using System.Text;

namespace CollectBiometrics
{
    public class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                if (args.Length != 0)
                {
                    if (args[0] == "1")
                        TakeFingerprint();

                    Environment.Exit(1);
                }
                else
                {
                    TakeFingerprint();
                    //Environment.Exit(3);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(0);
            }
        }

        #region Biometric Operations

        private static void TakeFingerprint()
        {
            string location = @"C:/InvestigatorErrorLog_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            var sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString("hh:mmtt"));
            sb.Append("==========");

            try
            {
                ScanningSettings sett = ScanningSettings.Current;
                sett.MinQuality = 50;
                sett.MinMinutiaCount = 30;
                sett.MinArea = 0;// Int32.Parse(ConfigurationManager.AppSettings["MinArea"].ToString());
                sett.MaxRotation = 30;
                sett.CaptureDelay = 300;
                sett.ShowGauges = true;

                sett.Futronic.ScanDose = 0;
                sett.Futronic.EliminateBackground = true;

                var finger = new Finger(FingerIndex.LeftThumb);
                var fingerScanForm = new FingerScanForm(finger);
                var showDialog = fingerScanForm.ShowDialog();
                if (showDialog != null && showDialog.Value)
                {
                    
                    Afisteam.Biometrics.Math.Innovatrics.InnovatricsImageConverter wsqConverter = new Afisteam.Biometrics.Math.Innovatrics.InnovatricsImageConverter();
                    byte[] wsq = wsqConverter.ConvertTo(finger.BestFingerprint.GetImageAsArray(), ImgFormat.WSQ);

                    // Save to server
                    int result = new InvestigatorProxy().InvestigatorChannel.SaveFingerprint(wsq);

                    if (result == 0)
                    {
                        sb.Append("Failed to save image to server via service");
                        sb.Append("");

                        if (File.Exists(location))
                        {
                            File.AppendAllText(location, sb.ToString());
                            sb.Clear();
                        }
                        else
                        {
                            File.WriteAllText(location, sb.ToString());
                        }
                    }
                }
                else
                {
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {                
                sb.Append(ex.Message);
                sb.Append("");

                if (File.Exists(location))
                {
                    File.AppendAllText(location, sb.ToString());
                    sb.Clear();
                }
                else
                {
                    File.WriteAllText(location, sb.ToString());
                }
            }
        }

        #endregion
    }
}
